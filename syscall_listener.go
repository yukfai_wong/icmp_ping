package main

import (
	"fmt"
	"os"
	"syscall"
)

func main() {
	fd, _ := syscall.Socket(syscall.AF_INET, syscall.SOCK_RAW, syscall.IPPROTO_ICMP)
	f := os.NewFile(uintptr(fd), fmt.Sprintf("fd %d", fd))

	for {
		buf := make([]byte, 1024)
		numRead, err := f.Read(buf)
		if err != nil {
			fmt.Println(err)
		}
    for i := 0; i < len(buf[:(numRead)]); i=i+4 {
      fmt.Printf("%2d  %08b\033[1;34m%08b\033[0m%08b\033[1;34m%08b\033[0m\n", i, buf[:numRead][i], buf[:numRead][i+1], buf[:numRead][i+2], buf[:numRead][i+3])
    }
		for i := 0; i < len(buf[:(numRead)]); i=i+1 {
      fmt.Printf("%d,", buf[:numRead][i])
    }
		fmt.Printf("\n")
		fmt.Printf("% X\n", buf[:numRead])
		fmt.Printf("%08b\033[1;34m%08b\033[0m\n", buf[:numRead][10], buf[:numRead][11])
		buf[:numRead][10] = 0
		buf[:numRead][11] = 0
		fmt.Printf("%016b\n", csum(buf[:numRead][:20]))

	}
}

func csum(b []byte) uint16 {
	var s uint32
	for i := 0; i < len(b); i += 2 {
		s += uint32(b[i+1])<<8 | uint32(b[i])
	}
	// add back the carry
	s = s>>16 + s&0xffff
	s = s + s>>16
	return uint16(^s)
}
