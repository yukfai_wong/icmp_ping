package main

import (
    "fmt"
    "net"
    "time"
    "log"
    "strings"
    "errors"
    "time"
)

func main() {
	conn, err := net.ListenIP("ip4:1", nil)
	if err != nil {
		log.Fatalf("ListenIP: %s\n", err)
	}
	var receiveTime time.Time
	for {
		buf := make([]byte, 1024)
		numRead, adr, err := conn.ReadFrom(buf)
		if err != nil {
			log.Fatalf("ReadFrom: %s\n", err)
		}

		receiveTime = time.Now()
    fmt.Println(receiveTime)
    fmt.Println(adr)
    fmt.Printf("Received: % s\n", string(buf[:numRead]))
		fmt.Printf("Received: % X\n", buf[:numRead])
    for i := 0; i < len(buf[:numRead]); i=i+4 {
			fmt.Printf("%2d  %08b\033[1;34m%08b\033[0m%08b\033[1;34m%08b\033[0m\n", i, buf[:numRead][i], buf[:numRead][i+1], buf[:numRead][i+2], buf[:numRead][i+3])
		}
	}
}

func outgoing_IP() (string, error){
	interfaces, err := net.Interfaces()
	if err != nil {
		fmt.Println("net.Interfaces: %s", err)
	}
	for _, iface := range interfaces {
		// Skip loopback
		if strings.Contains(iface.Name, "lo") {
			continue
		}
		addrs, err := iface.Addrs()
		// Skip if error getting addresses
		if err != nil {
			fmt.Println("Error get addresses for interfaces %s. %s", iface.Name, err)
			continue
		}
		if len(addrs) > 0 {
			// This one will do
			return addrs[0].String(), nil
		}
	}
	return "", errors.New("no usable network interfaces found")
}
