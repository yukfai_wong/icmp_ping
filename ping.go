package main

import (
	"os"
	"fmt"
	"net"
	"time"
	"syscall"
	"flag"
	"unsafe"
	"bytes"
	"encoding/binary"
)

var host	string
var ttl 	uint
var delay	int


func init() {
	flag.UintVar(&ttl, "m", 65, "manually set TTL")
	flag.IntVar(&delay, "t", 500, "manually set periodic microsecond delay")
	flag.Usage = usage
	flag.Parse()
	host = flag.Arg(0)
	if(host == ""){
		fmt.Println("Please enter host name or IPv4 address")
		usage()
	}
}

func usage() {
    fmt.Println("Usage: ping [-m TTL] [-t microsecond_delay] host")
    flag.PrintDefaults()
}


func main() {
	dst_IP := IPlookup(host)
	socket, addr := open_socket(dst_IP)
	go receiver(socket)

	var ICMP_packet []byte
	isIPv4 := (dst_IP.To4() != nil)
	for {
		if(isIPv4){
			ICMP_packet = IPv4_ICMP_const(dst_IP, uint32(ttl))
		}
		error := syscall.Sendto(socket, ICMP_packet, 0, &addr)
		if error != nil {
			fmt.Println("Sendto:", error)
			os.Exit(1)
		}
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}
}


func IPlookup(host string) net.IP {
	dst_IP := net.ParseIP(host)
	if(dst_IP != nil){
		return dst_IP
	}
	IP_list,err := net.LookupIP(host)
	if(err != nil){
		fmt.Println(err)
		os.Exit(1)
	}
	return IP_list[0]
}

func open_socket(ip net.IP) (int, syscall.SockaddrInet4){
	if(ip.To4() != nil){
		socket, err := syscall.Socket(syscall.AF_INET, syscall.SOCK_RAW, syscall.IPPROTO_ICMP)
		if err != nil {
      fmt.Println(err)
			os.Exit(1)
	  }
		syscall.SetsockoptInt(socket, syscall.IPPROTO_IP, syscall.IP_HDRINCL, 1)
		//The IPv4 layer generates an IP header when sending a packet unless the IP_HDRINCL socket option is enabled on the socket.
		addr := syscall.SockaddrInet4{
			Addr: [4]byte{ip[12], ip[13], ip[14], ip[15]},
			// Addr: [4]byte{127, 0, 0, 1},
		}
		return socket, addr
	} else {
		//Sorry, I don't support IPv6
		return 0, syscall.SockaddrInet4{}
	}
}

func receiver(fd int){
	f := os.NewFile(uintptr(fd), fmt.Sprintf("fd %d", fd))
	var RTT string

	for {
		buf := make([]byte, 1024)
		numRead, err := f.Read(buf)
		time_c := time.Now()
		if err != nil {
			fmt.Println(err)
		}
		switch response_control := *(*uint8)(unsafe.Pointer(&buf[:(numRead)][20])); response_control{
		case 11:
			fmt.Printf("%s: ICMP time exceeded in-transit with TTL %d\n", host, ttl)
		case 0:
			timestamp := time.Unix(0, int64(binary.BigEndian.Uint64(buf[:(numRead)][28:36])))//timestemp
			RTT = time_c.Sub(timestamp).String()
			fmt.Printf("%s: TTL: %d RTT: %s\n", host, ttl, RTT)
		default:
			fmt.Printf("%s: Unparse ICMP control code: %d\n", host, response_control)
		}
    // for i := 0; i < len(buf[:(numRead)]); i=i+4 {
    //   fmt.Printf("%2d  %08b\033[1;34m%08b\033[0m%08b\033[1;34m%08b\033[0m\n", i, buf[:numRead][i], buf[:numRead][i+1], buf[:numRead][i+2], buf[:numRead][i+3])
    // }
		// for i := 0; i < len(buf[:(numRead)]); i=i+1 {
    //   fmt.Printf("%d,", buf[:numRead][i])
    // }
		// fmt.Printf("\n")
		// fmt.Printf("% x\n", buf[:numRead])
	}
}

func IPv4_ICMP_const(dst net.IP, TTL uint32) []byte {
	ICMP_packet_buf := new(bytes.Buffer)
	var ICMP_type uint32 = uint32(8) << 24
	// var code uint32 = uint32(0) << 16
	// var code ICMP_checksum = uint32(0)
	binary.Write(ICMP_packet_buf, binary.BigEndian, ICMP_type)
	binary.Write(ICMP_packet_buf, binary.BigEndian, uint32(0))//Identifier	Sequence number
	c_t := time.Now()
	binary.Write(ICMP_packet_buf, binary.BigEndian, c_t.UnixNano())
	// fmt.Println(c_t.Unix())
	// binary.Write(ICMP_packet_buf, binary.BigEndian, c_t.UnixNano())
	ICMP_packet:=ICMP_packet_buf.Bytes()
	ICMP_checksum := csum(ICMP_packet)
	ICMP_packet[2] = byte(ICMP_checksum)
	ICMP_packet[3] = byte(ICMP_checksum>>8)

	IP_packet_buf := new(bytes.Buffer)
	var version uint32 = uint32(4) << 28
	var IHL uint32 = uint32(5) << 24
	var DSCP_ECN	 uint32 = uint32(192) << 16 //11000000
	var Total_length = uint32(20+len(ICMP_packet))
	binary.Write(IP_packet_buf, binary.BigEndian, version|IHL|DSCP_ECN|Total_length)//first 32bits
	//do not need to fragment to pack, the second 32bits is empty
	binary.Write(IP_packet_buf, binary.BigEndian, uint32(0))//second 32bits
	TTL = TTL << 24
	var Protocol uint32 = uint32(1) << 16 //value1: ICMP, Internet Control Message Protocol.
	//Header Checksum will be calculated in the end
	binary.Write(IP_packet_buf, binary.BigEndian, TTL|Protocol)//third 32bits
	binary.Write(IP_packet_buf, binary.BigEndian, uint32(0))//fourth 32bits
	binary.Write(IP_packet_buf, binary.BigEndian, IPv42uint32(dst))//fifth 32bits
	IP_packet := IP_packet_buf.Bytes()
	Checksum := csum(IP_packet)
	IP_packet[10] = byte(Checksum)
	IP_packet[11] = byte(Checksum>>8)
	binary.LittleEndian.PutUint16(IP_packet[2:4], uint16(20+len(ICMP_packet)))
	return append(IP_packet,ICMP_packet...)
}

func csum(b []byte) uint16 {
	var s uint32
	for i := 0; i < len(b); i += 2 {
		s += uint32(b[i+1])<<8 | uint32(b[i])
	}
	// add back the carry
	s = s>>16 + s&0xffff
	s = s + s>>16
	return uint16(^s)
}

func IPv42uint32(ip net.IP) uint32{
	// for b := range ip{
	// 	fmt.Printf("%d %d %b %b\n", b,ip[b], ip[b], uint32(ip[b]))
	// }
	return (uint32(ip[12])<<24 | uint32(ip[13])<<16 | uint32(ip[14])<<8 | uint32(ip[15]))
}
